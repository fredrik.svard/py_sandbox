class CodeBuilder:
    def __init__(self, root_name):
        self.root_name = root_name
        self.fields = []

    def add_field(self, type, name):
        elem = f"self.{type} = {name}\n"
        self.fields.append(elem)
        self

    def __str__(self):
        n = ''
        return f"class {self.root_name}:\n\
              def __init__(self):\
              {str([n.join(field) for field in self.fields])}"



cb = CodeBuilder('Person').add_field('name', '""')
print(cb)