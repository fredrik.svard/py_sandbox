import asyncio
from pprint import pprint

import aiohttp
import ssl

async def fetch_all(url, loop, count):
    async def fetch(session, url):
        async with session.get(url, ssl=ssl.SSLContext()) as response:
            return await response.json()

    async with aiohttp.ClientSession(loop=loop) as session:
        results = await asyncio.gather(*[fetch(session, url) for _ in range(count)], return_exceptions=True)

        return results


if __name__ == '__main__':
    url = 'https://jsonplaceholder.typicode.com/users'
    loop = asyncio.get_event_loop()
    htmls = loop.run_until_complete(fetch_all(url, loop, 5))
    pprint(htmls)
