import requests
import xml.etree.ElementTree as ET
import pprint

resp = requests.get('http://www.sltider.se/getDpsDepartures/3469')

pp = pprint.PrettyPrinter(indent=4)

pp.pprint(resp.content)

root = ET.fromstring(resp.content)

print (root.tag)

for child in root:
    print(child.tag, child.attrib)
