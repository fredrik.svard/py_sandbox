import requests
from hamcrest import *
# This is a sample Python script.

# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.


def print_hi(name):
    # Use a breakpoint in the code line below to debug your script.
    print(f'Hi, {name}')  # Press ⌘F8 to toggle the breakpoint.

class Olle:
    def __init__(self):
        self.nisse ='olle'
    def __str__(self):
        return "heja"


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    print_hi('PyCharm')
    resp = requests.get('http://example.com')
    print(resp.headers)

    # Object
    assert_that(True, equal_to(True))
    assert_that([1,2], has_length(2))
    olle = Olle()
    assert_that(olle, has_properties('nisse','olle'))
    assert_that(olle, has_string('heja'))
    assert_that(olle, instance_of(Olle))
    kalle = None
    #assert_that(kalle, equal_to(none))
    assert_that(olle, same_instance(olle))
    # https://github.com/hamcrest/PyHamcrest
   # https://python.hotexamples.com/examples/hamcrest/-/all_of/python-all_of-function-examples.html
    # Number

    # Text

    assert_that("i like cheese", contains_string("cheese"))

    # Logical

    assert_that(-1, all_of(greater_than_or_equal_to(-15), less_than_or_equal_to(20)))

    # Sequence

    # Dictionary

    # Decorator


# See PyCharm help at https://www.jetbrains.com/help/pycharm/
