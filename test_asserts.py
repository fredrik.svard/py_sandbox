import unittest
from hamcrest import *


class Olle:
    def __init__(self):
        self.nisse = 'olle'

    def __str__(self):
        return "heja"


class MyTestCase(unittest.TestCase):

    def setUp(self) -> None:

        self.olle = Olle()

    def test_object(self):
        assert_that(True)
        assert_that([1, 2], has_length(2))
        assert_that(self.olle, has_properties('nisse', 'olle'))
        assert_that(self.olle, has_string('heja'))
        assert_that(self.olle, instance_of(Olle))
        assert_that(self.olle, same_instance(self.olle))

    def test_text(self):
        assert_that(True)

    def test_number(self):
        assert_that(True)

    def test_logical(self):
        assert_that(15, all_of(greater_than(10), less_than(20)))

    def test_sequence(self):
        assert_that(True)

    def test_dictionary(self):
        assert_that(True)

    def test_decorator(self):
        assert_that(True)
        assert_that(True, equal_to(True))



if __name__ == '__main__':
    unittest.main()
